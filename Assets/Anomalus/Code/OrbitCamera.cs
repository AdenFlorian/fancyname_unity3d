﻿using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour {

	public bool rotate = true;
	public float rotateSpeed = 1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (rotate) {
			transform.Rotate(Vector3.up, Time.deltaTime * rotateSpeed);
		}
	
	}
}
